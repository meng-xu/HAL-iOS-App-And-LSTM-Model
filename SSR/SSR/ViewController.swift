//
//  ViewController.swift
//  SSR
//
//  Created by Meng Xu on 11/19/17.
//  Copyright © 2017 Meng Xu. All rights reserved.
//

import UIKit
import AVFoundation
import CoreBluetooth

class ViewController: UIViewController,
  UITableViewDelegate, UITableViewDataSource,
  CBCentralManagerDelegate, CBPeripheralDelegate {

  // constants
  let SSR_NAME = "SSR"

  let SSR_SOUND_BEEP:SystemSoundID = 1013

  // devices
  let SSR_KNOWN_DEVICES:Set<String> = [
      "25008906-BAE2-442D-BBCC-933110D815BD", // 0
      "56121C5C-7A0B-821D-9262-13AA9C7FFC4D", // 1
      "E22DAE7A-AEB2-BDF3-D4E0-056E2A5AA206", // 2
      "03FB9FBA-549E-0173-88B5-E17F88E38F0C", // 3
      "11834C99-DF46-0975-9CC7-AD2CC87CC844", // 4
      "52588145-0EAB-D8A3-EC13-A4A56E30CD65", // 5
      "B2D4867C-62A9-4DF8-9ADF-B22461B83280", // 6
      "9EB8EA0A-356C-25C1-B025-640AC372CF64", // 7
      "70EBA439-4262-F235-E580-C0EF759CE695", // 8
      "713FA7C2-D6BD-338D-70A2-8E651E7D232E", // 9
      "833EBE99-92B8-10DB-93A4-76F17F7B6BCD", // 10
      "4531B297-5904-93D2-B2B8-2D78CC0C4E4B", // 11
      "925EBB96-CE33-C8BD-85D6-4A86B94E404E", // 12
  ]

  let SSR_COLOR_DEVICES:[String:UIColor] =
    [
      "25008906-BAE2-442D-BBCC-933110D815BD": ChartColors.greenColor(),     // 0
      "56121C5C-7A0B-821D-9262-13AA9C7FFC4D": ChartColors.blueColor(),      // 1
      "E22DAE7A-AEB2-BDF3-D4E0-056E2A5AA206": ChartColors.cyanColor(),      // 2
      "03FB9FBA-549E-0173-88B5-E17F88E38F0C": ChartColors.darkGreenColor(), // 3
      "11834C99-DF46-0975-9CC7-AD2CC87CC844": ChartColors.darkRedColor(),   // 4
      "52588145-0EAB-D8A3-EC13-A4A56E30CD65": ChartColors.goldColor(),      // 5
      "B2D4867C-62A9-4DF8-9ADF-B22461B83280": ChartColors.greyColor(),      // 6
      "9EB8EA0A-356C-25C1-B025-640AC372CF64": ChartColors.maroonColor(),    // 7
      "70EBA439-4262-F235-E580-C0EF759CE695": ChartColors.orangeColor(),    // 8
      "713FA7C2-D6BD-338D-70A2-8E651E7D232E": ChartColors.pinkColor(),      // 9
      "833EBE99-92B8-10DB-93A4-76F17F7B6BCD": ChartColors.purpleColor(),    // 10
      "4531B297-5904-93D2-B2B8-2D78CC0C4E4B": ChartColors.redColor(),       // 11
      "925EBB96-CE33-C8BD-85D6-4A86B94E404E": ChartColors.yellowColor(),    // 12
  ]

  // timing
  let SSR_PREP_TIME = 5

  // devices
  var DEVICES:[CBUUID] = []

  // manager
  var manager:CBCentralManager!

  // activity
  var action:String = ""
  var span:Int = 0
  var inter:Int = 0
  var count:Int = 0

  var timer:Timer = Timer()
  var timerRunning = false

  // data
  var timestamp:Int64 = 0
  var records:[String:[Int64:Int]] = [:]

  // history
  var saved:[String] = []

  // UI
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var scanSwitch: UISwitch!
  @IBOutlet weak var actionField: UITextField!
  @IBOutlet weak var intervalField: UITextField!
  @IBOutlet weak var countField: UITextField!
  @IBOutlet weak var startButton: UIButton!
  @IBOutlet weak var actionLabel: UILabel!
  @IBOutlet weak var intervalLabel: UILabel!
  @IBOutlet weak var countLabel: UILabel!
  @IBOutlet weak var histTable: UITableView!
  let histCellReuseIdentifier = "hist-cell"
  @IBOutlet weak var scanChart: Chart!
  typealias XYTuple = (x: Float, y: Float)

  // view life cycles
  override func viewDidLoad() {
    super.viewDidLoad()

    // config misc
    let GESTURE_TAB = UITapGestureRecognizer(target: self,
                                             action: #selector(self.dismissKeyboard (_:)))
    self.view.addGestureRecognizer(GESTURE_TAB)

    // create UUID
    for dev in SSR_KNOWN_DEVICES {
      DEVICES.append(CBUUID(string:dev))
    }

    // create font
    let FONT_LABEL:UIFont = UIFont(name: "HelveticaNeue-Thin", size: CGFloat(18.0))!

    // config status label
    statusLabel.font = FONT_LABEL
    statusLabel.text = "Bluetooth LE Signal Strength Reader"

    // config activity label
    actionLabel.font = FONT_LABEL
    intervalLabel.font = FONT_LABEL
    countLabel.font = FONT_LABEL

    // config hist table
    histTable.delegate = self
    histTable.dataSource = self
    histTable.register(UITableViewCell.self,
                       forCellReuseIdentifier: histCellReuseIdentifier)

    // config activity defaults
    resetActivityFields()

    // collect saved records
    collectHists()

    // init manager
    manager = CBCentralManager(delegate: self, queue: nil)

    // config scan switch
    scanSwitch.setOn(false, animated: false)
    scanSwitch.addTarget(self,
                         action: #selector(ViewController.scanSwitchChanged(_:)),
                         for: UIControlEvents.valueChanged)
    scanSwitch.isHidden = true

    // init records
    for dev in SSR_KNOWN_DEVICES {
      records[dev] = [Int64:Int]()
    }

    // config chart
    scanChart.minY = -100
    scanChart.maxY = 0

    // take timestamp
    timestamp = currentTimeMillis()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  // number of rows in table view
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return saved.count
  }

  // create a cell for each table view row
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell:UITableViewCell =
      histTable.dequeueReusableCell(withIdentifier: histCellReuseIdentifier)
        as UITableViewCell!

    let comps = saved[indexPath.row].components(separatedBy: "-")
    if comps.count != 2 {
      showAlert("Invalid name found for saved records")
      cell.textLabel?.text = saved[indexPath.row]
    } else {
      let date = Date(timeIntervalSince1970: TimeInterval(Int64(comps[0])! / 1000000))
      let formatter = DateFormatter()
      formatter.dateFormat = "MMM-dd HH:mm:ss"
      let dstr = formatter.string(from: date)
      cell.textLabel?.text = dstr + "\t\t" + comps[1]
    }

    return cell
  }

  // on table row select
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if !drawAllSeries(saved[indexPath.row]) {
      showAlert("Failed to draw saved records")
    }
  }

  // activity logics
  @IBAction func onActivityStart(_ sender: UIButton) {
    // input validation
    if actionField.text?.isEmpty ?? true {
      showAlert("Invalid action")
      return
    }
    action = actionField.text!

    if intervalField.text?.isEmpty ?? true {
      showAlert("Invalid interval")
      return
    }
    if Int(intervalField.text!) == nil {
      showAlert("Invalid interval")
      return
    }
    span = Int(intervalField.text!)!
    if span <= 0 {
      showAlert("Invalid interval")
      return
    }

    if countField.text?.isEmpty ?? true {
      showAlert("Invalid count")
      return
    }
    if Int(countField.text!) == nil {
      showAlert("Invalid count")
      return
    }
    count = Int(countField.text!)!
    if count <= 0 {
      showAlert("Invalid count")
      return
    }

    // prepare interface
    actionField.isUserInteractionEnabled = false
    actionField.backgroundColor = UIColor.lightGray

    intervalField.isUserInteractionEnabled = false
    intervalField.backgroundColor = UIColor.green

    countField.isUserInteractionEnabled = false
    countField.backgroundColor = UIColor.cyan

    // schedule timer
    inter = SSR_PREP_TIME
    count += 1

    if !timerRunning {
      timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                                   selector: #selector(activityTimerRunning),
                                   userInfo: nil, repeats: true)
      timerRunning = true
    }

    // set status
    statusLabel.text = "Activity Testing"
  }

  @objc func activityTimerRunning() {
    // update interval
    inter -= 1
    intervalField.text = String(inter)

    if inter == 0 {
      // process current scan
      stopDeviceScan()

      // update count
      count -= 1
      countField.text = String(count)
      inter = span

      if count == 0 {
        // invalidate timer
        timer.invalidate()
        timerRunning = false

        // reset fields
        resetActivityFields()
        statusLabel.text = "Done"
      } else {
        // start a new cycle of scanning
        startDeviceScan()
        AudioServicesPlaySystemSound(SSR_SOUND_BEEP)
      }
    }
  }

  func resetActivityFields() {
    actionField.text = ""
    actionField.isUserInteractionEnabled = true
    actionField.backgroundColor = UIColor.clear

    intervalField.text = "5"
    intervalField.isUserInteractionEnabled = true
    intervalField.backgroundColor = UIColor.clear

    countField.text = "50"
    countField.isUserInteractionEnabled = true
    countField.backgroundColor = UIColor.clear
  }

  // bluetooth logics
  func checkDevice() -> Bool {
    // check Bluetooth LE status
    var message = ""
    var ready:Bool = false

    switch manager.state {
    case .poweredOff:
      message = "Bluetooth LE on this device is currently powered off."
      break
    case .unsupported:
      message = "This device does not support Bluetooth LE."
      break
    case .unauthorized:
      message = "This app is not authorized to use Bluetooth LE."
      break
    case .resetting:
      message = "The Bluetooth LE Manager is resetting."
      break
    case .unknown:
      message = "The state of the Bluetooth LE Manager is unknown."
      break
    case .poweredOn:
      ready = true
      break
    }

    // show alert
    if !ready {
      let alert = UIAlertController(
        title: "Bluetooth LE State",
        message: message,
        preferredStyle: UIAlertControllerStyle.alert)

      let action = UIAlertAction(
        title: "OK",
        style: UIAlertActionStyle.cancel,
        handler: nil)

      alert.addAction(action)
      self.show(alert, sender: self)
    }

    return ready
  }

  func startDeviceScan() {
    // clear records
    for (k, _) in records {
      records[k] = [Int64:Int]()
    }

    // take timestamp
    timestamp = currentTimeMillis()

    // start scanning
    manager.scanForPeripherals(withServices: nil,
                               options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
  }

  func stopDeviceScan() {
    // stop scanning
    manager.stopScan()

    // save records
    if !saveRecords() {
      showAlert("Data not saved!")
    }

    // reload records
    collectHists()
    histTable.reloadData()
  }

  // bluetooth callbacks
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    if checkDevice() {
      statusLabel.text = "Ready"
    } else {
      statusLabel.text = "[!] Unavailable"
    }
  }

  // core callbacks
  @objc func scanSwitchChanged(_ sender : UISwitch) {
    if sender.isOn {
      // start scanning if not already
      if !manager.isScanning {
        if checkDevice() {
          statusLabel.text = "Scanning..."
          startDeviceScan()
        } else {
          scanSwitch.setOn(false, animated: true)
          statusLabel.text = "[!] Unavailable"
        }
      }
    } else {
      if manager.isScanning {
        stopDeviceScan()
        statusLabel.text = "Stopped"
      }
    }
  }

  // on peripheral discovered
  func centralManager(
    _ central: CBCentralManager,
    didDiscover peripheral: CBPeripheral,
    advertisementData: [String : Any],
    rssi RSSI: NSNumber) {

    let uuid:String = peripheral.identifier.uuidString
    let rssi:Int = RSSI.intValue
    let time:Int64 = currentTimeMillis() - timestamp

    // filter out abnormal cases
    if rssi > 0 {
      return
    }

    // filter out unknown devices
    if !SSR_KNOWN_DEVICES.contains(uuid) {
      return
    }

    // record the data
    records[uuid]![time] = rssi;
  }

  // records saving and loading
  func saveRecords() -> Bool {
    // calc path
    let dirp = (FileManager.default.urls(for: .documentDirectory,
                                         in: .userDomainMask)).last

    let dstr = String(format: "%lu-%@", timestamp, action)
    let docp = dirp?.appendingPathComponent(dstr)

    if FileManager.default.fileExists(atPath: (docp?.path)!) {
      return false
    }

    // serialize data
    var serialized:String = ""
    for (k, v) in records {
      serialized += String(format: "%@:%d\n", k, v.count)
      for (ts, si) in v {
        serialized += String(format: "%d:%d\n", ts, si)
      }
    }

    // save data
    do {
      try serialized.write(to: docp!,
                           atomically: true,
                           encoding: String.Encoding.ascii)
    } catch {
      return false
    }

    return true
  }

  func collectHists() {
    // clear up
    saved.removeAll()

    // list document directory
    let dirp = (FileManager.default.urls(for: .documentDirectory,
                                         in: .userDomainMask)).last

    // get the directory contents urls (including subfolders urls)
    let dirc = try! FileManager.default.contentsOfDirectory(at: dirp!,
                                                            includingPropertiesForKeys: nil,
                                                            options: [])
    for fp in dirc {
      saved.append(fp.lastPathComponent)
    }
  }

  // visualization
  func drawOneSeries(_ recs: [Int64:Int], _ color: UIColor) {
    let tseq = recs.sorted(by: { $0.0 < $1.0 })
    var data:[XYTuple] = []
    for (k, v) in tseq {
      data.append((x: Float(Int64(k) / 1000000), y: Float(v)))
    }

    // only draw when there is more than one point
    if data.count != 0 {
      let series = ChartSeries(data: data)
      series.color = color
      scanChart.add(series)
    }
  }

  func drawAllSeries(_ fname: String) -> Bool {
    // calc path
    let dirp = (FileManager.default.urls(for: .documentDirectory,
                                         in: .userDomainMask)).last

    let docp = dirp?.appendingPathComponent(fname)

    if !FileManager.default.fileExists(atPath: (docp?.path)!) {
      return false
    }

    do {
      // load data
      var histrec:[String:[Int64:Int]] = [:]

      let data = try String(contentsOf: docp!, encoding: String.Encoding.ascii)
      let lines = data.components(separatedBy: "\n")

      var i:Int = 0
      while i < lines.count - 1 {
        let toks = lines[i].components(separatedBy: ":")
        histrec[toks[0]] = [:]
        i += 1

        let len:Int = Int(toks[1])!
        var j:Int = 0
        while j < len {
          let splits = lines[i + j].components(separatedBy: ":")
          histrec[toks[0]]![Int64(splits[0])!] = Int(splits[1])
          j += 1
        }

        i += len
      }

      // draw each series
      scanChart.removeAllSeries()
      for (k, v) in histrec {
        drawOneSeries(v, SSR_COLOR_DEVICES[k]!)
      }

      return true
    } catch {
      return false
    }
  }

  // utils
  func currentTimeMillis() -> Int64 {
    var darwinTime : timeval = timeval(tv_sec: 0, tv_usec: 0)
    gettimeofday(&darwinTime, nil)
    return (Int64(darwinTime.tv_sec) * 1000000) + Int64(darwinTime.tv_usec)
  }

  func showAlert(_ message: String) {
    let alert = UIAlertController(
      title: "Error",
      message: message,
      preferredStyle: UIAlertControllerStyle.alert)

    let action = UIAlertAction(
      title: "OK",
      style: UIAlertActionStyle.cancel,
      handler: nil)

    alert.addAction(action)
    self.show(alert, sender: self)
  }

  @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
    actionField.resignFirstResponder()
    intervalField.resignFirstResponder()
    countField.resignFirstResponder()
  }
}

