#!/usr/bin/env python

import numpy as np
import tensorflow as tf

from parse import LABELS, load_data, load_test

# configs (as a class)
class Config(object):

    def __init__(self, X_train, X_test, Y_labels):
        # input data
        self.train_count = len(X_train)
        self.test_count = len(X_test)
        self.n_steps = len(X_train[0])

        # training
        self.learning_rate = 0.001
        self.lambda_loss_amount = 0.0015
        self.training_epochs = 500
        self.batch_size = 5

        # model
        self.n_inputs = len(X_train[0][0])
        self.n_hidden = 8
        self.n_classes = len(Y_labels)
        self.W = {
                'hidden': tf.Variable(tf.random_normal([self.n_inputs, self.n_hidden])),
                'output': tf.Variable(tf.random_normal([self.n_hidden, self.n_classes]))
                }
        self.biases = {
                'hidden': tf.Variable(tf.random_normal([self.n_hidden], mean=1.0)),
                'output': tf.Variable(tf.random_normal([self.n_classes]))
                }

# network layout
def LSTM_Network(_X, config):
    # reshape data
    _X = tf.transpose(_X, [1, 0, 2])
    _X = tf.reshape(_X, [-1, config.n_inputs])

    # linear activation
    _X = tf.nn.relu(tf.matmul(_X, config.W['hidden']) + config.biases['hidden'])
    _X = tf.split(_X, config.n_steps, 0)

    lstm_cell_1 = tf.contrib.rnn.BasicLSTMCell(config.n_hidden, forget_bias=1.0, state_is_tuple=True)
    lstm_cell_2 = tf.contrib.rnn.BasicLSTMCell(config.n_hidden, forget_bias=1.0, state_is_tuple=True)
    lstm_cells = tf.contrib.rnn.MultiRNNCell([lstm_cell_1, lstm_cell_2], state_is_tuple=True)

    # get cell output
    outputs, states = tf.contrib.rnn.static_rnn(lstm_cells, _X, dtype=tf.float32)

    # linear activation 
    lstm_last_output = outputs[-1]
    return tf.matmul(lstm_last_output, config.W['output']) + config.biases['output']

# main
if __name__ == "__main__":

    # -----------------------------
    # Step 1: load and prepare data
    # -----------------------------

    X_train, Y_train, X_test, Y_test = load_data()

    # -----------------------------------
    # Step 2: define parameters for model
    # -----------------------------------

    config = Config(X_train, X_test, LABELS)

    # ------------------------------------------------------
    # Step 3: let's get serious and build the neural network
    # ------------------------------------------------------

    X = tf.placeholder(tf.float32, [None, config.n_steps, config.n_inputs])
    Y = tf.placeholder(tf.float32, [None, config.n_classes])

    pred_Y = LSTM_Network(X, config)

    # loss, optimizer, eval
    l2 = config.lambda_loss_amount * sum(tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables())
    cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=pred_Y)) + l2
    optimizer = tf.train.AdamOptimizer(learning_rate=config.learning_rate).minimize(cost)

    correct_pred = tf.equal(tf.argmax(pred_Y, 1), tf.argmax(Y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, dtype=tf.float32))

    # --------------------------------------------
    # Step 4: hooray, now train the neural network
    # --------------------------------------------

    sess = tf.InteractiveSession(config=tf.ConfigProto(log_device_placement=False))
    init = tf.global_variables_initializer()
    sess.run(init)

    # start training
    best_accuracy = 0.0
    for i in range(config.training_epochs):
        for start, end in zip(range(0, config.train_count, config.batch_size),
                range(config.batch_size, config.train_count + 1, config.batch_size)):

            sess.run(optimizer, feed_dict={X: X_train[start:end], Y: Y_train[start:end]})

        # test at each epoch
        pred_out, accuracy_out, loss_out = sess.run(
                [pred_Y, accuracy, cost],
                feed_dict={
                    X: X_test,
                    Y: Y_test
                    }
                )

        print("traing iter: {},".format(i) +
                " test accuracy : {},".format(accuracy_out) +
                " loss : {}".format(loss_out))
        best_accuracy = max(best_accuracy, accuracy_out)

    print("")
    print("final test accuracy: {}".format(accuracy_out))
    print("best epoch's test accuracy: {}".format(best_accuracy))
    print("")

    # --------------------------------------------
    # Step 5: final testing
    # --------------------------------------------

    TX, TY = load_test()
    
    pred_out, accuracy_out, loss_out = sess.run(
            [pred_Y, accuracy, cost],
            feed_dict={
                X: TX,
                Y: TY
                }
            )

    print("======== TY ========")
    for i in TY:
        print(i)
    print("")

    print("======== PY ========")
    for i in pred_out:
        print(i)
    print("")

    print("ACTUAL TEST accuracy : {},".format(accuracy_out) + 
            " loss : {}".format(loss_out))
